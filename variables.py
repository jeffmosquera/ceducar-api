

CREDENCIALES_INCORRECTAS = "Credenciales incorrectas, por favor verifique su usuario y contraseña."

ACCESO_DENEGADO = "Acceso denegado"


# Departamento
DEPARTAMENTO_CREADO = "Departamento creado con éxito."
DEPARTAMENTO_NOMBRE_YA_EXISTE = "Un departamento con ese nombre ya existe."
DEPARTAMENTO_NO_ECONTRADO = "No se ha logrado encontrar el departamento."
DEPARTAMENTO_ACTUALIZADO = "Se ha actualizado el departamento."
DEPARTAMENTO_ELIMINADO = "Se ha eliminado el departamento."


# Usuario
USUARIO_CREADO = "Usuario creado con éxito."
USUARIO_CEDULA_YA_EXISTE = "Un usuario con ese número de cedula ya existe."
USUARIO_NO_ECONTRADO = "No se ha logrado encontrar el usuario."
USUARIO_ACTUALIZADO = "Se ha actualizado el usuario."
USUARIO_ELIMINADO = "Se ha eliminado el usuario."


ADMINISTRADOR_NO_ECONTRADO = "No se ha logrado encontrar el administrador."
