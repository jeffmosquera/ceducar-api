from flask_restplus import Model, fields

esquema_departamento = Model('Store', {
    '_id': fields.String,
    'nombre': fields.String,
    'descripcion': fields.String,
    'activo': fields.Boolean,
    'actualizado_en': fields.String,
    'creado_en': fields.String,
})

todo_esquema_departamento = Model('Todo el esquema ', {
    'data': fields.List(fields.Nested(esquema_departamento))
})
