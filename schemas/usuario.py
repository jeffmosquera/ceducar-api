from flask_restplus import Model, fields

esquema_usuario = Model('Store', {
    '_id': fields.String,
    'nombre': fields.String,
    'correo': fields.String,
    'cedula': fields.String,
    'telefono': fields.String,
    'clave': fields.String,
    'huella_id': fields.String,
    # 'activo': fields.Boolean,
    # 'actualizado_en': fields.String,
    # 'creado_en': fields.String,
})

todo_esquema_usuario = Model('Todo el esquema ', {
    'data': fields.List(fields.Nested(esquema_usuario))
})


