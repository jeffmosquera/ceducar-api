from flask_restplus import Model, fields
from common.util import BinaryBase64


store_schema = Model('Store', {
    '_id': fields.String,
    'name': fields.String,
    'created_at': fields.Float,
    'updated_at': fields.Float,
    'activated': fields.Boolean,
    'description': fields.String,
    'video_link': fields.String,
    'image_file': BinaryBase64
})

store_schema_all = Model('All Store', {
    'data': fields.List(fields.Nested(store_schema))
})
