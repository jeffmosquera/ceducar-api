from flask_restplus import Resource, reqparse, Namespace, marshal, inputs
from models.usuario import ModeloUsuario
from models.marcacion import ModeloMarcacion
from schemas.usuario import *
from schemas.marcacion import *
from variables import *
from common.correo import enviar_correo_por_registro
from common.util import TimeLocation

modelo_usuario = ModeloUsuario()
modelo_marcacion = ModeloMarcacion()

api = Namespace('marcaciones', description='Funciones de marcaciones.')


marcacion_parser = api.parser()
marcacion_parser.add_argument('huella_id', type=str, required=True, location="form")


class Entrada(Resource):
    
    # @jwt_required
    def get(self):
        """
        Obtener todas las marcaciones de entrada
        """
        # if not(get_jwt_identity()['rol'] in ['administrador','user']):
        #     return {'msg': ACCESO_DENEGADO}, 401
        marcaciones = modelo_marcacion.encontrar_todo_entrada()
        marcaciones = marshal({'data': marcaciones}, todo_esquema_marcacion, skip_none=True)
        return marcaciones, 200

    @api.doc(parser=marcacion_parser)
    def post(self):
        """
        Registrar entrada
        """
        data = marcacion_parser.parse_args()
        usuario = modelo_usuario.encontrar_por_huella_id(data['huella_id'])

        if usuario == None:
            return {'msg': "Usuario no registrado"}, 400

        fecha = TimeLocation.get()
        enviar_correo_por_registro(usuario['nombre'], usuario['correo'], "entrada", fecha)
        data['usuario_id'] = str(usuario['_id'])
        data['creado_en'] = fecha
        _id = modelo_marcacion.guardar_entrada(data)
        usuario = marshal(usuario, esquema_usuario)
        return {'data': usuario, 'msg': "Registro de entrada con éxito"}, 200
    
class Salida(Resource):

    # @jwt_required
    def get(self):
        """
        Obtener todas las marcaciones de salida
        """
        # if not(get_jwt_identity()['rol'] in ['administrador','user']):
        #     return {'msg': ACCESO_DENEGADO}, 401
        marcaciones = modelo_marcacion.encontrar_todo_salida()
        marcaciones = marshal({'data': marcaciones}, todo_esquema_marcacion, skip_none=True)
        return marcaciones, 200
    
    @api.doc(parser=marcacion_parser)
    def post(self):
        """
        Registrar salida
        """
        data = marcacion_parser.parse_args()
        usuario = modelo_usuario.encontrar_por_huella_id(data['huella_id'])

        if usuario == None:
            return {'msg': "Usuario no registrado"}, 400

        fecha = TimeLocation.get()
        enviar_correo_por_registro(usuario['nombre'], usuario['correo'], "salida", fecha)
        data['usuario_id'] = str(usuario['_id'])
        data['creado_en'] = fecha
        _id = modelo_marcacion.guardar_salida(data)
        usuario = marshal(usuario, esquema_usuario)
        return {'data': usuario, 'msg': "Registro de salida con éxito"}, 200


class SalidaAlmuerzo(Resource):

    # @jwt_required
    def get(self):
        """
        Obtener todas las marcaciones de salida de almuerzo
        """
        # if not(get_jwt_identity()['rol'] in ['administrador','user']):
        #     return {'msg': ACCESO_DENEGADO}, 401
        marcaciones = modelo_marcacion.encontrar_todo_salida_almuerzo()
        marcaciones = marshal({'data': marcaciones}, todo_esquema_marcacion, skip_none=True)
        return marcaciones, 200


    @api.doc(parser=marcacion_parser)
    def post(self):
        """
        Registrar entrada
        """
        data = marcacion_parser.parse_args()
        usuario = modelo_usuario.encontrar_por_huella_id(data['huella_id'])

        if usuario == None:
            return {'msg': "Usuario no registrado"}, 400

        fecha = TimeLocation.get()
        enviar_correo_por_registro(usuario['nombre'], usuario['correo'], "salida de almuerzo", fecha)
        data['usuario_id'] = str(usuario['_id'])
        data['creado_en'] = fecha
        _id = modelo_marcacion.guardar_salida_almuerzo(data)
        usuario = marshal(usuario, esquema_usuario)
        return {'data': usuario, 'msg': "Registro de salida de almuerzo con éxito"}, 200

class RegresoAlmuerzo(Resource):

    # @jwt_required
    def get(self):
        """
        Obtener todas las marcaciones de regreso de almuerzo
        """
        # if not(get_jwt_identity()['rol'] in ['administrador','user']):
        #     return {'msg': ACCESO_DENEGADO}, 401
        marcaciones = modelo_marcacion.encontrar_todo_regreso_almuerzo()
        marcaciones = marshal({'data': marcaciones}, todo_esquema_marcacion, skip_none=True)
        return marcaciones, 200


    
    @api.doc(parser=marcacion_parser)
    def post(self):
        """
        Registrar entrada
        """
        data = marcacion_parser.parse_args()
        usuario = modelo_usuario.encontrar_por_huella_id(data['huella_id'])

        if usuario == None:
            return {'msg': "Usuario no registrado"}, 400

        fecha = TimeLocation.get()
        enviar_correo_por_registro(usuario['nombre'], usuario['correo'], "regreso de almuerzo", fecha)
        data['usuario_id'] = str(usuario['_id'])
        data['creado_en'] = fecha
        _id = modelo_marcacion.guardar_regreso_almuerzo(data)
        usuario = marshal(usuario, esquema_usuario)
        return {'data': usuario, 'msg': "Registro de entrada de almuerzo con éxito"}, 200

class Marcacion(Resource):
    
    # @jwt_required
    def get(self):
        """
        Obtener todas las marcaciones
        """
        # if not(get_jwt_identity()['rol'] in ['administrador','user']):
        #     return {'msg': ACCESO_DENEGADO}, 401
        marcaciones = modelo_marcacion.encontrar_todo()
        marcaciones = marshal({'data': marcaciones}, todo_esquema_marcacion, skip_none=True)
        return marcaciones, 200

api.add_resource(Entrada, '/entrada')
api.add_resource(Salida, '/salida')
api.add_resource(SalidaAlmuerzo, '/salida_almuerzo')
api.add_resource(RegresoAlmuerzo, '/regreso_almuerzo')

api.add_resource(Marcacion, '')
