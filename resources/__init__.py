from flask_restplus import Api

# Importar librerias para manejo de errores
from flask_jwt_extended.exceptions import NoAuthorizationError, RevokedTokenError
from jwt.exceptions import ExpiredSignatureError, DecodeError
from pymongo.errors import AutoReconnect

from resources.administrador import api as administrador_api
from resources.usuario import api as usuario_api
from resources.departamento import api as departamento_api
from resources.marcacion import api as marcacion_api
from resources.reporte import api as reporte_api



api = Api(
    title='Ceducar API',
    version='1.0',
    description='API para el control total del sistema',
    catch_all_404s=True     # Manejar el error de todas las direcciones 404
)

api.add_namespace(administrador_api)
api.add_namespace(usuario_api)
api.add_namespace(marcacion_api)
api.add_namespace(reporte_api)



# Manejo de errores
@api.errorhandler(RevokedTokenError)
@api.errorhandler(NoAuthorizationError)
@api.errorhandler(ExpiredSignatureError)
@api.errorhandler(DecodeError)
@api.errorhandler(AutoReconnect)
def handle_error():
    pass