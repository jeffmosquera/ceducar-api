from flask_restplus import Resource, reqparse, Namespace, marshal, inputs
from flask_jwt_extended import jwt_required, get_jwt_identity
from werkzeug.security import safe_str_cmp
from models.usuario import ModeloUsuario
from models.marcacion import ModeloMarcacion
from schemas.usuario import *
from schemas.marcacion import *
from variables import *
import datetime
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_refresh_token_required,
    get_jwt_identity,
    jwt_required,
    get_raw_jwt,
)
from common.correo import enviar_correo, send_report
from common.CrearArchivo import usuarios_excel, marcaciones_excel, entrada_excel, salida_almuerzo_excel, regreso_almuerzo_excel, salida_excel

modelo_usuario = ModeloUsuario()
modelo_marcacion = ModeloMarcacion()

api = Namespace('reportes', description='Funciones para los usuarios de la empresa.')


reporte_parser = api.parser()
reporte_parser.add_argument('correo', type=str, required=True, location="form")


class Usuario(Resource):
    
    @api.doc(parser=reporte_parser)
    def post(self):
        """
        Enviar reporte de usuarios al correo
        """
        data = reporte_parser.parse_args()

        usuarios = modelo_usuario.encontrar_todo()
        usuarios_excel('usuarios', usuarios)


        send_report(data['correo'], 'usuarios')
        return {'msg': "Reporte de usuarios enviado al correo"}, 200


class Marcacion(Resource):
    
    @api.doc(parser=reporte_parser)
    def post(self):
        """
        Enviar reporte de marcaciones al correo
        """
        data = reporte_parser.parse_args()

        marcaciones = modelo_marcacion.encontrar_todo()
        marcaciones_excel('marcaciones', marcaciones)


        send_report(data['correo'], 'marcaciones')
        return {'msg': "Reporte de marcaciones enviado al correo"}, 200
    

class Entrada(Resource):
    
    @api.doc(parser=reporte_parser)
    def post(self):
        """
        Enviar reporte de entrada al correo
        """
        data = reporte_parser.parse_args()

        marcaciones = modelo_marcacion.encontrar_todo_entrada()
        entrada_excel('entrada', marcaciones)


        send_report(data['correo'], 'entrada')
        return {'msg': "Reporte de entrada enviado al correo"}, 200

class SalidaAlmuerzo(Resource):
    
    @api.doc(parser=reporte_parser)
    def post(self):
        """
        Enviar reporte de salida de almuerzo al correo
        """
        data = reporte_parser.parse_args()

        marcaciones = modelo_marcacion.encontrar_todo_salida_almuerzo()
        salida_almuerzo_excel('salida_almuerzo', marcaciones)


        send_report(data['correo'], 'salida_almuerzo')
        return {'msg': "Reporte de salida de almuerzo enviado al correo"}, 200


class RegresoAlmuerzo(Resource):
    
    @api.doc(parser=reporte_parser)
    def post(self):
        """
        Enviar reporte de regreso de almuerzo al correo
        """
        data = reporte_parser.parse_args()

        marcaciones = modelo_marcacion.encontrar_todo_regreso_almuerzo()
        regreso_almuerzo_excel('regreso_almuerzo', marcaciones)


        send_report(data['correo'], 'regreso_almuerzo')
        return {'msg': "Reporte de regreso de almuerzo enviado al correo"}, 200

class Salida(Resource):
    
    @api.doc(parser=reporte_parser)
    def post(self):
        """
        Enviar reporte de salida al correo
        """
        data = reporte_parser.parse_args()

        marcaciones = modelo_marcacion.encontrar_todo_salida()
        salida_excel('salida', marcaciones)


        send_report(data['correo'], 'salida')
        return {'msg': "Reporte de salida enviado al correo"}, 200


api.add_resource(Usuario, '/usuarios')
api.add_resource(Marcacion, '/marcaciones')
api.add_resource(Entrada, '/entrada')
api.add_resource(SalidaAlmuerzo, '/salida_almuerzo')
api.add_resource(RegresoAlmuerzo, '/regreso_almuerzo')
api.add_resource(Salida, '/salida')
