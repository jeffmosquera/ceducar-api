from flask_restplus import Resource, reqparse, Namespace, fields, marshal
from models.administrador import ModeloAdministrador
from werkzeug.security import safe_str_cmp
from variables import *
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_refresh_token_required,
    get_jwt_identity,
    jwt_required,
    get_raw_jwt,
)
from blacklist import BLACKLIST
from variables import *
from schemas.administrador import *
import datetime

modelo_administrador = ModeloAdministrador()

api = Namespace('administradores', description='Funciones de sesión para el administrador')

ADMIN_LOGGED_OUT = "Administrador desconectado con exitoso."
ADMIN_LOGIN = "Inicio de sesion exitoso."

login_schema = api.model('LoginAdministrator', {
    '_id': fields.String,
    'email': fields.String,
    'password': fields.String
})

admin_login_parser = api.parser()
admin_login_parser.add_argument('correo', type=str, required=True)
admin_login_parser.add_argument('clave', type=str, required=True)

token_parser = api.parser()
token_parser.add_argument(
    'Authorization', type=str,
    location='headers',
    help='Bearer Access Token',
    required=True
)

token_refresh_parser = api.parser()
token_refresh_parser.add_argument(
    'Authorization', type=str,
    location='headers',
    help='Bearer Token Refresh',
    required=True
)


class AdministradorLogin(Resource):

    @api.doc(parser=admin_login_parser)
    def post(self):
        """
        Acceso login para administrador
        """
        data = admin_login_parser.parse_args()
        administrador = modelo_administrador.encontrar_por_correo(data["correo"])
        print(data)
        print(administrador)
        if administrador:
            if safe_str_cmp(data["clave"], administrador['clave']):
                expires = datetime.timedelta(days=5)
                access_token = create_access_token(
                    identity = {
                        '_id': str(administrador['_id']),
                        'rol': 'administrador'
                    }, fresh = True, expires_delta=expires
                )
                refresh_token = create_refresh_token(
                    {
                        '_id': str(administrador['_id']),
                        'rol': 'administrador'
                    }
                )
                return {
                    'data': {
                        "access_token": access_token, "refresh_token": refresh_token
                    }
                }, 200
        return {"msg": CREDENCIALES_INCORRECTAS}, 401


class AdministratorLogout(Resource):
    
    @api.doc(parser=token_parser)
    @jwt_required
    def post(self):
        """
        Logout para administrador
        """
        jti = get_raw_jwt()["jti"]  # jti is "JWT ID", a unique identifier for a JWT.
        admin_id = get_jwt_identity()
        BLACKLIST.add(jti)
        return {"msg": ADMIN_LOGGED_OUT}, 200


class TokenRefresh(Resource):
    
    @api.doc(parser=token_refresh_parser)
    @jwt_refresh_token_required
    def post(self):
        """
        Actualizar token para administrador
        """
        current_admin = get_jwt_identity()
        new_token = create_access_token(identity=current_admin, fresh=False)
        return {
            'data': {"access_token": new_token},
            'msg': current_admin
        }, 200

class AdministradorPorID(Resource):
    
    @jwt_required
    def get(self, administrador_id):
        """
        Obtener un administrador buscado por ID
        """
        if not(get_jwt_identity()['rol'] in ['administrador']):
            return {'msg': ACCESO_DENEGADO}, 401

        administrador = modelo_administrador.encontrar_por_id(administrador_id)
        
        if administrador == None:
            return {'msg': ADMINISTRADOR_NO_ECONTRADO}, 404
        return marshal(administrador, esquema_administrador, envelope='data'), 200



api.add_resource(AdministradorPorID, '/<string:administrador_id>')
api.add_resource(AdministradorLogin, '/login')
api.add_resource(AdministratorLogout, '/logout')
api.add_resource(TokenRefresh, '/actulizartoken')
