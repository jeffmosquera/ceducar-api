from flask_restplus import Resource, reqparse, Namespace, marshal, inputs
from flask_jwt_extended import jwt_required, get_jwt_identity
from models.departamento import ModeloDepartamento
from schemas.departamento import *
from variables import *

modelo_departamento = ModeloDepartamento()

api = Namespace('departamentos', description='Funciones para los departamentos de la empresa.')


departamento_parser = api.parser()
departamento_parser.add_argument('nombre', type=str, required=True, location="form")
departamento_parser.add_argument('descripcion', type=str, required=True, location="form")

class Departamento(Resource):
    
    # @jwt_required
    def get(self):
        """
        Obtener todos los departamentos activos
        """
        # if not(get_jwt_identity()['rol'] in ['administrador','user']):
        #     return {'msg': ACCESO_DENEGADO}, 401
        departamentos = modelo_departamento.encontrar_todo()
        departamentos = marshal({'data': departamentos}, todo_esquema_departamento, skip_none=True)
        return departamentos, 200

    @jwt_required
    @api.doc(parser=departamento_parser)
    def post(self):
        """
        Crear un nuevo departamento
        """
        if not(get_jwt_identity()['rol'] in ['administrador']):
            return {'msg': ACCESO_DENEGADO}, 401
        data = departamento_parser.parse_args()

        if modelo_departamento.encontrar_por_nombre(data['nombre']):
            return {'msg': DEPARTAMENTO_NOMBRE_YA_EXISTE}

        _id = modelo_departamento.guardar_en_db(data)
        return {'data': {'departamento_id': _id}, 'msg': DEPARTAMENTO_CREADO}, 201
    

class DepartamentoPorID(Resource):
    
    @jwt_required
    def get(self, departamento_id):
        """
        Obtener un departamento buscado por ID
        """
        if not(get_jwt_identity()['rol'] in ['administrador','user']):
            return {'msg': ACCESO_DENEGADO}, 401

        departamento = modelo_departamento.encontrar_por_id(departamento_id)
        
        if departamento == None:
            return {'msg': DEPARTAMENTO_NO_ECONTRADO}, 404
        return marshal(departamento, esquema_departamento, envelope='data'), 200
    

    @jwt_required
    @api.doc(parser=departamento_parser)
    def put(self, departamento_id):
        """
        Actualizar un departamento buscado por ID
        """
        if not(get_jwt_identity()['rol'] in ['administrador']):
            return {'msg': ACCESO_DENEGADO}, 401

        data = departamento_parser.parse_args()
        if modelo_departamento.encontrar_por_id(departamento_id) == None:
            return {'msg': DEPARTAMENTO_NO_ECONTRADO}, 404

        if modelo_departamento.encontrar_por_nombre_excepto_id(data['nombre'], departamento_id):
            return {'msg': DEPARTAMENTO_NOMBRE_YA_EXISTE}

        modelo_departamento.actualizar_por_id(departamento_id, data)
        return {'msg': DEPARTAMENTO_ACTUALIZADO}, 200


    @jwt_required
    def delete(self, departamento_id):
        """
        Eliminar un departamento buscado por ID
        """
        if not(get_jwt_identity()['rol'] in ['administrador']):
            return {'msg': ACCESO_DENEGADO}, 401

        if modelo_departamento.encontrar_por_id(departamento_id) == None:
            return {'msg': DEPARTAMENTO_NO_ECONTRADO}, 404

        modelo_departamento.eliminar_por_id(departamento_id)
        return {'msg': DEPARTAMENTO_ELIMINADO}, 200


api.add_resource(Departamento, '')
api.add_resource(DepartamentoPorID, '/<string:departamento_id>')
