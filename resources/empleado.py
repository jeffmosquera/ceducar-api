from flask_restplus import Resource, reqparse, Namespace, marshal, inputs
from flask_jwt_extended import jwt_required, get_jwt_identity
from models.store import StoreModel
from schemas.store import *
from variables import *

from werkzeug.datastructures import FileStorage
from common.util import AllowedExtensions


store_model = StoreModel()


# Namespace de la Tienda
api = Namespace('stores', description='Funciones para la tienda.')


store_parser = api.parser()
store_parser.add_argument('name', type=str, required=True, location="form")
store_parser.add_argument('description', type=str, required=True, location="form")
store_parser.add_argument('video_link', type=str, required=True, location="form")
store_parser.add_argument('image_file',
                         type=FileStorage,
                         location='files',
                         required=True,
                         help='Image file')

class Store(Resource):
    
    @jwt_required
    def get(self):
        """
        Obtener todos los afiliados
        """
        if not(get_jwt_identity()['rol'] in ['admin','user']):
            return {'msg': NO_ACCESS}, 401
        stores = store_model.find_all()
        stores = marshal({'data': stores}, store_schema_all, skip_none=True)
        return stores, 200

    @jwt_required
    @api.doc(parser=store_parser)
    def post(self):
        """
        Crear un nuevo afiliado
        """
        if not(get_jwt_identity()['rol'] in ['admin']):
            return {'msg': NO_ACCESS}, 401
        data = store_parser.parse_args()
        if store_model.find_by_name(data['name']):
            return {'msg': STORE_ALREADY_EXISTS}, 400
        file = data['image_file']
        if file and AllowedExtensions.verify(file.filename):
            data['image_file'] = file.read()
            _id = store_model.save_to_db(data)
            return {'data': {'store_id': _id}, 'msg': STORE_CREATED_SUCCESSFULLY}, 201
        return {'msg': STORE_IMAGE_FORMAT_NO_ALLOWED}


class StoreByID(Resource):
    
    @jwt_required
    def get(self, store_id):
        """
        Obtener un afiliado
        """
        if not(get_jwt_identity()['rol'] in ['admin','user']):
            return {'msg': NO_ACCESS}, 401
        store = store_model.find_by_id(store_id)
        if store == None:
            return {'msg': STORE_NOT_FOUND}, 404
        return marshal(store, store_schema, envelope='data'), 200
    

    @jwt_required
    @api.doc(parser=store_parser_update)
    def put(self, store_id):
        """
        Actualizar un afiliado
        """
        if not(get_jwt_identity()['rol'] in ['admin']):
            return {'msg': NO_ACCESS}, 401

        data = store_parser_update.parse_args()
        if store_model.find_by_id(store_id) == None:
            return {'msg': STORE_NOT_FOUND}, 404
        if store_model.find_another_by_name(data['name'], store_id):
            return {'msg': STORE_ALREADY_EXISTS}, 400

        file = data['image_file']
        if file:
            if AllowedExtensions.verify(file.filename) == False:
                return {'msg': STORE_IMAGE_FORMAT_NO_ALLOWED}
            data['image_file'] = file.read()
        else:
            data.pop('image_file', None)
        
        if data['activated'] == None:
            data.pop('activated', None)
        
        # data = marshal(data, store_schema, skip_none=True)
        store_model.update_by_id(store_id, data)
        return {'msg': STORE_UPDATED}, 200


    # @jwt_required
    # def delete(self, store_id):
    #     """
    #     Eliminar un afiliado
    #     """
    #     if store_model.find_by_id(store_id) == None:
    #         return {'msg': STORE_NOT_FOUND}, 404
    #     store_model.delete_by_id(store_id)
    #     return {'msg': STORE_DELETED}, 200


api.add_resource(Store, '')
api.add_resource(StoreByID, '/<string:store_id>')
