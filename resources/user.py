from flask_restplus import Resource, reqparse, Namespace, fields, marshal
from flask_jwt_extended import jwt_required
from models.user import UserModel
from models.city import CityModel

user_model = UserModel()
city_model = CityModel()

api = Namespace('users', description='Users')


USER_ALREADY_EXISTS = "Un usuario con ese correo ya existe."
USER_CREATED_SUCCESSFULLY = "Usuario creado con exito."
USER_NOT_FOUND = "No se encuentra el usuario."
USER_UPDATED = "Usuario actualizado."
USER_DELETED = "Usuario eliminado."


login_schema = api.model('Login', {
    '_id': fields.String,
    'email': fields.String,
    'password': fields.String
})


user_parser = api.parser()
user_parser.add_argument('name', type=str, required=True)
user_parser.add_argument('last_name', type=str, required=True)
user_parser.add_argument('age', type=int, required=True)
user_parser.add_argument('email', type=str, required=True)
user_parser.add_argument('username', type=str, required=True)
user_parser.add_argument('password', type=str, required=True)
user_parser.add_argument('city_id', type=str, required=True)
user_parser.add_argument('phone', type=str, required=True)

store_parser = api.parser()
store_parser.add_argument('name', type=str, required=True)
store_parser.add_argument('description', type=str, required=True)

class User(Resource):


    @api.doc(parser=user_parser)
    def post(self):
        """
        Registro de usuario
        """
        data = user_parser.parse_args()


        if city_model.find_by_id(data['city_id']) == None:
            return {'msg': "Ciudad no encontrada"}, 404

        
        if user_model.find_by_email(data['email']):
            return {'msg': USER_ALREADY_EXISTS}, 400


        user_model.save_to_db(data)
        return {'msg': USER_CREATED_SUCCESSFULLY}, 200
        
        




user_logout_parser = api.parser()
user_logout_parser.add_argument(
    'Authorization', type=str,
    location='headers',
    help='Bearer Access Token',
    required=True
)



class UserLogout(Resource):
    
    
    
    @api.doc(parser=user_logout_parser)
    @jwt_required
    def post(self):
        """
        User logout
        """
        jti = get_raw_jwt()["jti"]  # jti is "JWT ID", a unique identifier for a JWT.
        print(jti)
        user_id = get_jwt_identity()
        print(user_id)
        BLACKLIST.add(jti)
        print(BLACKLIST)
        return {"message": USER_LOGGED_OUT % user_id}, 200



token_refresh_parser = api.parser()
token_refresh_parser.add_argument(
    'Authorization', type=str,
    location='headers',
    help='Bearer Token Refresh',
    required=True
)

class TokenRefresh(Resource):
    
    @api.doc(parser=token_refresh_parser)
    def post(self):
        """
        Token refresh
        """
        current_user = get_jwt_identity()
        new_token = create_access_token(identity=current_user, fresh=False)
        return {"access_token": new_token}, 200


api.add_resource(User, '')
api.add_resource(UserLogout, '/logout')
api.add_resource(TokenRefresh, '/token_refresh')


