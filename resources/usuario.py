from flask_restplus import Resource, reqparse, Namespace, marshal, inputs
from flask_jwt_extended import jwt_required, get_jwt_identity
from werkzeug.security import safe_str_cmp
from models.usuario import ModeloUsuario
from models.marcacion import ModeloMarcacion
from models.departamento import ModeloDepartamento
from schemas.usuario import *
from schemas.marcacion import *
from variables import *
import datetime
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_refresh_token_required,
    get_jwt_identity,
    jwt_required,
    get_raw_jwt,
)
from common.correo import enviar_correo

modelo_usuario = ModeloUsuario()
modelo_marcacion = ModeloMarcacion()
modelo_departamento = ModeloDepartamento()

api = Namespace('usuarios', description='Funciones para los usuarios de la empresa.')


usuario_parser = api.parser()
usuario_parser.add_argument('nombre', type=str, required=True, location="form")
usuario_parser.add_argument('correo', type=str, required=True, location="form")
usuario_parser.add_argument('cedula', type=str, required=True, location="form")
usuario_parser.add_argument('telefono', type=str, required=True, location="form")
usuario_parser.add_argument('clave', type=str, required=True, location="form")
# usuario_parser.add_argument('departamento_id', type=str, required=True, location="form")
usuario_parser.add_argument('huella_id', type=str, required=True, location="form")


class Usuario(Resource):
    
    # @jwt_required
    def get(self):
        """
        Obtener todos los usuarios activos
        """
        # if not(get_jwt_identity()['rol'] in ['administrador']):
        #     return {'msg': ACCESO_DENEGADO}, 401
        usuarios = modelo_usuario.encontrar_todo()
        usuarios = marshal({'data': usuarios}, todo_esquema_usuario, skip_none=True)
        return usuarios, 200

    @jwt_required
    @api.doc(parser=usuario_parser)
    def post(self):
        """
        Crear un nuevo usuario
        """
        if not(get_jwt_identity()['rol'] in ['administrador']):
            return {'msg': ACCESO_DENEGADO}, 401
        data = usuario_parser.parse_args()

        if modelo_usuario.encontrar_por_cedula(data['cedula']):
            return {'msg': USUARIO_CEDULA_YA_EXISTE}
        
        # if modelo_departamento.encontrar_por_id(data['departamento_id']) == None:
        #     return {'msg': DEPARTAMENTO_NO_ECONTRADO}, 404

        enviar_correo(data)
        _id = modelo_usuario.guardar_en_db(data)
        return {'msg': USUARIO_CREADO}, 201
    

class UsuarioPorID(Resource):
    
    # @jwt_required
    def get(self, usuario_id):
        """
        Obtener un departamento buscado por ID
        """
        # if not(get_jwt_identity()['rol'] in ['administrador','user']):
        #     return {'msg': ACCESO_DENEGADO}, 401

        
        
        usuario = modelo_usuario.encontrar_por_id(usuario_id)
        
        if usuario == None:
            return {'msg': USUARIO_NO_ECONTRADO}, 404
        return marshal(usuario, esquema_usuario, envelope='data'), 200
    

    @jwt_required
    @api.doc(parser=usuario_parser)
    def put(self, usuario_id):
        """
        Actualizar un departamento buscado por ID
        """
        if not(get_jwt_identity()['rol'] in ['administrador']):
            return {'msg': ACCESO_DENEGADO}, 401

        data = usuario_parser.parse_args()
        if modelo_usuario.encontrar_por_id(usuario_id) == None:
            return {'msg': USUARIO_NO_ECONTRADO}, 404

        # if modelo_departamento.encontrar_por_id(data['departamento_id']) == None:
        #     return {'msg': DEPARTAMENTO_NO_ECONTRADO}, 404
        
        if modelo_usuario.encontrar_por_cedula_excepto_id(data['cedula'], usuario_id):
            return {'msg': USUARIO_CEDULA_YA_EXISTE}

        modelo_usuario.actualizar_por_id(usuario_id, data)
        return {'msg': USUARIO_ACTUALIZADO}, 200


    @jwt_required
    def delete(self, usuario_id):
        """
        Eliminar un usuario buscado por ID
        """
        if not(get_jwt_identity()['rol'] in ['administrador']):
            return {'msg': ACCESO_DENEGADO}, 401

        if modelo_usuario.encontrar_por_id(usuario_id) == None:
            return {'msg': USUARIO_NO_ECONTRADO}, 404

        modelo_usuario.eliminar_por_id(usuario_id)
        return {'msg': USUARIO_ELIMINADO}, 200


class Marcaciones(Resource):
    
    def get(self, usuario_id):
        """
        Obtener las marcaciones del usuario
        """

        usuario = modelo_usuario.encontrar_por_id(usuario_id)
        
        if usuario == None:
            return {'msg': USUARIO_NO_ECONTRADO}, 404
        
        
        marcaciones = modelo_marcacion.encontrar_todo_por_usuario_id(usuario_id)
        marcaciones = marshal({'data': marcaciones}, todo_esquema_marcacion, skip_none=True)
        return marcaciones, 200


usuario_login_parser = api.parser()
usuario_login_parser.add_argument('correo', type=str, required=True)
usuario_login_parser.add_argument('clave', type=str, required=True)

class UsuarioLogin(Resource):

    @api.doc(parser=usuario_login_parser)
    def post(self):
        """
        Acceso login para administrador
        """
        data = usuario_login_parser.parse_args()
        usuario = modelo_usuario.encontrar_por_correo(data["correo"])
        print(usuario)
        print(data)
        if usuario:
            if safe_str_cmp(data["clave"], usuario['clave']):
                expires = datetime.timedelta(days=5)
                access_token = create_access_token(
                    identity = {
                        '_id': str(usuario['_id']),
                        'rol': 'usuario'
                    }, fresh = True, expires_delta=expires
                )
                refresh_token = create_refresh_token(
                    {
                        '_id': str(usuario['_id']),
                        'rol': 'usuario'
                    }
                )
                return {
                    'data': {
                        "access_token": access_token, "refresh_token": refresh_token
                    }
                }, 200
        return {"msg": CREDENCIALES_INCORRECTAS}, 401


api.add_resource(Usuario, '')
api.add_resource(UsuarioPorID, '/<string:usuario_id>')
api.add_resource(Marcaciones, '/<string:usuario_id>/marcaciones')
api.add_resource(UsuarioLogin, '/login')

