from database import DataMongo
from bson.objectid import ObjectId
from common.util import TimeLocation


class StoreModel():
    
    def __init__(self):
        self.db = DataMongo.connect()
        self.collection = self.db.stores

    def find_all(self):
        docs = []
        for doc in self.collection.find({'activated': True}).sort('name'):
            docs.append(doc)
        return docs

    def save_to_db(self, data):
        data['created_at'] = TimeLocation.get()
        data['updated_at'] = TimeLocation.get()
        data['activated'] = True
        _id = self.collection.insert_one(data)
        return str(_id.inserted_id)

    def find_by_name(self, name):
        doc = self.collection.find_one({'name': name, 'activated': True})
        return doc

    def find_another_by_name(self, name, store_id):
        doc = self.collection.find_one({'name': name, 'activated': True})
        if doc:
            if str(doc['_id']) != store_id:
                return doc
        return None

    def find_by_id(self, _id):
        if ObjectId.is_valid(_id):
            doc = self.collection.find_one({'_id': ObjectId(_id), 'activated': True})
            return doc
        else:
            return None
    
    def update_by_id(self, _id, data):
        data['updated_at'] = TimeLocation.get()
        query = {'_id': ObjectId(_id)}
        newValues = { '$set': data }
        self.collection.update(query, newValues, upsert=True)

    # def delete_by_id(self, _id):
    #     self.collection.delete_one({'_id': ObjectId(_id)})
