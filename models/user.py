from database import DataMongo
from datetime import datetime
from bson.objectid import ObjectId

class UserModel():
    
    def __init__(self):
        self.db = DataMongo.connect()
        self.collection = self.db.users


    def find_all(self):
        docs = self.collection.find()
        return docs
    

    def save_to_db(self, data):
        now = datetime.utcnow()
        data['created_at'] = datetime.timestamp(now)
        data['activated'] = True
        _id = self.collection.insert_one(data)
        return str(_id.inserted_id)

    def find_by_name(self, name):

        doc = self.collection.find_one({
            'name': name
        })

        return doc
    
    def find_by_email(self, email):

        doc = self.collection.find_one({
            'email': email
        })

        return doc
    

    def find_by_id(self, _id):
        if ObjectId.is_valid(_id):
            doc = self.collection.find_one({
                '_id': ObjectId(_id)
            })
            return doc
        else:
            return None
    
    def update_by_id(self, _id, data):
        
        query = {'_id': ObjectId(_id)}
        newValues = { "$set": data }

        self.collection.update_one(query, newValues)

    def delete_by_id(self, _id):
        
        self.collection.delete_one({
            '_id': ObjectId(_id)
        })
