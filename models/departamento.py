from database import DataMongo
from datetime import datetime
from bson.objectid import ObjectId

class ModeloDepartamento():
    
    def __init__(self):
        self.db = DataMongo.connect()
        self.collection = self.db.departamentos


    def encontrar_todo(self):
        docs = []
        for doc in self.collection.find({'activo': True}):
            docs.append(doc)
        return docs
    

    def guardar_en_db(self, data):
        now = datetime.utcnow()
        data['actualizado_en'] = datetime.now()
        data['creado_en'] = datetime.now()
        data['activo'] = True
        _id = self.collection.insert_one(data)
        return str(_id.inserted_id)

    def encontrar_por_nombre(self, nombre):

        doc = self.collection.find_one({
            'nombre': nombre,
            'activo': True
        })

        return doc

    def encontrar_por_nombre_excepto_id(self, nombre, departamento_id):

        doc = self.collection.find_one({
            'nombre': nombre,
            'activo': True
        })

        if str(doc['_id']) == departamento_id:
            return None
        
        return doc
    
    

    def encontrar_por_id(self, _id):

        if ObjectId.is_valid(_id):
            store = self.collection.find_one({
                '_id': ObjectId(_id),
                'activo': True
            })

            return store
        else:
            return None
    

    def actualizar_por_id(self, _id, data):
        
        data['actualizado_en'] = datetime.now()
        query = {'_id': ObjectId(_id)}
        newValues = { "$set": data }

        self.collection.update_one(query, newValues)

    def eliminar_por_id(self, _id):
        
        data = {}
        data['updated_at'] = datetime.now()
        data['activo'] = False
        query = {'_id': ObjectId(_id)}
        newValues = { '$set': data }
        self.collection.update(query, newValues, upsert=True)
