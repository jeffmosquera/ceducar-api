from database import DataMongo
from datetime import datetime
from bson.objectid import ObjectId

class ModeloUsuario():
    
    def __init__(self):
        self.db = DataMongo.connect()
        self.collection = self.db.usuarios


    def encontrar_todo(self):
        docs = []
        for doc in self.collection.find({'activo': True}):
            docs.append(doc)
        return docs
    

    def guardar_en_db(self, data):
        now = datetime.utcnow()
        data['actualizado_en'] = datetime.now()
        data['creado_en'] = datetime.now()
        data['activo'] = True
        _id = self.collection.insert_one(data)
        return str(_id.inserted_id)


    def encontrar_por_cedula_excepto_id(self, cedula, usuario_id):

        doc = self.collection.find_one({
            'cedula': cedula,
            'activo': True
        })

        if str(doc['_id']) == usuario_id:
            return None
        
        return doc
    
    
    def encontrar_por_cedula(self, cedula):
        doc = self.collection.find_one({'cedula': cedula, 'activo': True})
        return doc

    def encontrar_por_huella_id(self, huella_id):
        doc = self.collection.find_one({'huella_id': huella_id, 'activo': True})
        return doc

    def encontrar_por_id(self, _id):

        if ObjectId.is_valid(_id):
            usuario = self.collection.find_one({
                '_id': ObjectId(_id),
                'activo': True
            })

            return usuario
        else:
            return None

    def encontrar_por_correo(self, correo):
        doc = self.collection.find_one({'correo': correo, 'activo': True})
        return doc    

    def actualizar_por_id(self, _id, data):
        
        data['actualizado_en'] = datetime.now()
        query = {'_id': ObjectId(_id)}
        newValues = { "$set": data }

        self.collection.update_one(query, newValues)

    def eliminar_por_id(self, _id):
        
        data = {}
        data['updated_at'] = datetime.now()
        data['activo'] = False
        query = {'_id': ObjectId(_id)}
        newValues = { '$set': data }
        self.collection.update(query, newValues, upsert=True)
