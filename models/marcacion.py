from database import DataMongo
from datetime import datetime
from bson.objectid import ObjectId
from common.util import TimeLocation
from models.usuario import ModeloUsuario
import pymongo

modelo_usuario = ModeloUsuario()



class ModeloMarcacion():
    
    def __init__(self):
        self.db = DataMongo.connect()
        self.collection = self.db.marcaciones
        self.entradas = self.db.entradas
        self.salidas = self.db.salidas
        self.salidas_almuerzo = self.db.salidas_almuerzo
        self.regresos_almuerzo = self.db.regresos_almuerzo


    def encontrar_todo(self):
        docs = []
        for doc in self.collection.find().sort('creado_en', pymongo.DESCENDING):
            doc['usuario'] = modelo_usuario.encontrar_por_id(doc['usuario_id'])
            docs.append(doc)
        return docs

    def encontrar_todo_por_usuario_id(self, usuario_id):
        docs = []
        for doc in self.collection.find({'usuario_id': usuario_id}).sort('creado_en', pymongo.DESCENDING):
            doc['usuario'] = modelo_usuario.encontrar_por_id(doc['usuario_id'])
            docs.append(doc)
        return docs
    

    def guardar_entrada(self, data):
        self.entradas.insert_one(data)
        data['tipo'] = "Entrada"
        self.collection.insert_one(data)

    def guardar_salida(self, data):
        self.salidas.insert_one(data)
        data['tipo'] = "Salida"
        self.collection.insert_one(data)

    def guardar_salida_almuerzo(self, data):
        self.salidas_almuerzo.insert_one(data)
        data['tipo'] = "Salida Almuerzo"
        self.collection.insert_one(data)

    def guardar_regreso_almuerzo(self, data):
        self.regresos_almuerzo.insert_one(data)
        data['tipo'] = "Regreso Almuerzo"
        self.collection.insert_one(data)


    def encontrar_todo_entrada(self):
        docs = []
        for doc in self.entradas.find().sort('creado_en', pymongo.DESCENDING):
            doc['usuario'] = modelo_usuario.encontrar_por_id(doc['usuario_id'])
            docs.append(doc)
        return docs


    def encontrar_todo_salida(self):
        docs = []
        for doc in self.salidas.find().sort('creado_en', pymongo.DESCENDING):
            doc['usuario'] = modelo_usuario.encontrar_por_id(doc['usuario_id'])
            docs.append(doc)
        return docs


    def encontrar_todo_salida_almuerzo(self):
        docs = []
        for doc in self.salidas_almuerzo.find().sort('creado_en', pymongo.DESCENDING):
            doc['usuario'] = modelo_usuario.encontrar_por_id(doc['usuario_id'])
            docs.append(doc)
        return docs


    def encontrar_todo_regreso_almuerzo(self):
        docs = []
        for doc in self.regresos_almuerzo.find().sort('creado_en', pymongo.DESCENDING):
            doc['usuario'] = modelo_usuario.encontrar_por_id(doc['usuario_id'])
            docs.append(doc)
        return docs

































    def encontrar_por_cedula_excepto_id(self, cedula, usuario_id):

        doc = self.collection.find_one({
            'cedula': cedula,
            'activo': True
        })

        if str(doc['_id']) == usuario_id:
            return None
        
        return doc
    
    
    def encontrar_por_cedula(self, cedula):
        doc = self.collection.find_one({'cedula': cedula, 'activo': True})
        return doc

    def encontrar_por_huella_id(self, huella_id):
        doc = self.collection.find_one({'huella_id': huella_id, 'activo': True})
        return doc

    def encontrar_por_id(self, _id):

        if ObjectId.is_valid(_id):
            store = self.collection.find_one({
                '_id': ObjectId(_id),
                'activo': True
            })

            return store
        else:
            return None
    

    def actualizar_por_id(self, _id, data):
        
        data['actualizado_en'] = datetime.now()
        query = {'_id': ObjectId(_id)}
        newValues = { "$set": data }

        self.collection.update_one(query, newValues)

    def eliminar_por_id(self, _id):
        
        data = {}
        data['updated_at'] = datetime.now()
        data['activo'] = False
        query = {'_id': ObjectId(_id)}
        newValues = { '$set': data }
        self.collection.update(query, newValues, upsert=True)
