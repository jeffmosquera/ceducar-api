from database import DataMongo
from datetime import datetime
from bson.objectid import ObjectId

class ModeloAdministrador():
    
    def __init__(self):
        self.db = DataMongo.connect()
        self.collection = self.db.administradores


    def encontrar_todo(self):
        docs = self.collection.find()
        return docs
    

    def guardar_en_db(self, data):
        now = datetime.utcnow()
        data['created_at'] = datetime.timestamp(now)
        data['activated'] = True
        _id = self.collection.insert_one(data)
        return str(_id.inserted_id)

    def encontrar_por_nombre(self, name):

        doc = self.collection.find_one({
            'name': name
        })

        return doc
    
    def encontrar_por_correo(self, correo):
        doc = self.collection.find_one({'correo': correo, 'activado': True})
        return doc
    

    def encontrar_por_id(self, _id):

        if ObjectId.is_valid(_id):
            store = self.collection.find_one({
                '_id': ObjectId(_id)
            })

            return store
        else:
            return None
    

    def actualizar_por_id(self, _id, data):
        
        query = {'_id': ObjectId(_id)}
        newValues = { "$set": data }

        self.collection.update_one(query, newValues)

    def borrar_por_id(self, _id):
        
        self.collection.delete_one({'_id': ObjectId(_id)})
