import email.message
import smtplib
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import formatdate
from email import encoders


def enviar_correo(data):
    server = smtplib.SMTP('smtp.gmail.com:587')

    email_content = """
    <html>
    <body>
        <nav class="navbar navbar-expand-md navbar-light  p-0 w-100" align="center" style="background-color:#000000;padding-bottom: 20px; text-align: center">
        </nav>
        <div>
            <h1 align="center" style="font-size:400%">Bienvenido, """+data['nombre']+"""</h1>
            <h3 align="center">Datos para poder acceder a la plataforma de Ceducar.</h3>
        </div>
        <div>
            <h3 align="center">Correo: """+data['correo']+"""</h3>
            <h3 align="center">Clave: """+data['clave']+"""</h3>
        </div>
    </body>
    </html>

    """

    msg = email.message.Message()
    msg['Subject'] = 'Registro con éxito'

    msg['From'] = 'sistemaceducar2019@gmail.com'
    msg['To'] = data['correo']
    password = "Ceduc@r2019"
    msg.add_header('Content-Type', 'text/html')
    msg.set_payload(email_content.encode('utf-8'))

    s = smtplib.SMTP('smtp.gmail.com:587')
    s.starttls()

    # Login Credentials for sending the mail
    s.login(msg['From'], password)

    s.sendmail('CEDUCAR', [msg['To']], msg.as_string())


def enviar_correo_por_registro(nombre, correo, tipo, fecha):
    server = smtplib.SMTP('smtp.gmail.com:587')

    email_content = """
    <html>
    <body>
        <nav class="navbar navbar-expand-md navbar-light  p-0 w-100" align="center" style="background-color:#000000;padding-bottom: 20px; text-align: center">
        </nav>
        <div>
            <h1 align="center" style="font-size:400%">Hola, """+nombre+"""</h1>
            <h3 align="center">Usted ha realizado un registro de """+tipo+""" en el sistema biometrico de Ceducar.</h3>
        </div>
        <div>
            <h3 align="center">Fecha-Hora: """+str(fecha)[:19]+"""</h3>
        </div>
    </body>
    </html>

    """

    msg = email.message.Message()
    msg['Subject'] = 'Registro con éxito'

    msg['From'] = 'sistemaceducar2019@gmail.com'
    msg['To'] = correo
    password = "Ceduc@r2019"
    msg.add_header('Content-Type', 'text/html')
    msg.set_payload(email_content.encode('utf-8'))

    s = smtplib.SMTP('smtp.gmail.com: 587')
    s.starttls()

    # Login Credentials for sending the mail
    s.login(msg['From'], password)

    s.sendmail('CEDUCAR', [msg['To']], msg.as_string())


def send_report(correo, filename):
    msg = MIMEMultipart()
    msg['From'] = 'Ceducar'
    msg['To'] = correo
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = "Reporte " + filename
    msg.attach(MIMEText("Reportes de usuarios"))

    part = MIMEBase('application', "octet-stream")
    part.set_payload(open('/root/api/reportes/' +
                          filename+'.xlsx', "rb").read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition',
                    'attachment; filename="Reporte.xlsx"')
    msg.attach(part)

    #context = ssl.SSLContext(ssl.PROTOCOL_SSLv3)
    # SSL connection only working on Python 3+
    smtp = smtplib.SMTP('smtp.gmail.com:587')
    # if isTls:
    smtp.starttls()
    smtp.login('sistemaceducar2019@gmail.com', 'Ceduc@r2019')
    smtp.sendmail(msg['From'], msg['To'], msg.as_string())
    smtp.quit()

# enviar_correo({'nombre': 'Jefferson', 'correo': 'jeffmosq93@gmail.com', 'clave': 'qwer1234'})
