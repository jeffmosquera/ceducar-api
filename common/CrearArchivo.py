import xlsxwriter


def usuarios_excel(filename, usuarios):

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook('/root/api/reportes/'+str(filename)+'.xlsx')
    worksheet = workbook.add_worksheet('usuarios')

    # Some data we want to write to the worksheet.
    subtitles = [
        'ID',
        'Nombre',
        'Correo',
        'Cedula',
        'Teléfono',
        'Clave',
        'Huella ID'
    ]

    worksheet.set_column('A:A', 30)
    worksheet.set_column('B:B', 25)
    worksheet.set_column('C:C', 45)
    worksheet.set_column('D:D', 15)
    worksheet.set_column('E:E', 15)
    worksheet.set_column('F:F', 20)
    worksheet.set_column('G:G', 20)

    format_title = workbook.add_format({
        'bold': True,
        'bg_color': '#ffd700',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 30
    })

    format_subtitle = workbook.add_format({
        'bold': True,
        'bg_color': '#00cece',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 20
    })

    format_id = workbook.add_format({
        'bg_color': '#d1af78',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_nombre = workbook.add_format({
        'bg_color': '#42ba93',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_correo = workbook.add_format({
        'bg_color': '#ffa500',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_cedula = workbook.add_format({
        'bg_color': '#ffc966',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_telefono = workbook.add_format({
        'bg_color': '#ffedcc',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_clave = workbook.add_format({
        'bg_color': '#4ca64c',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_huella = workbook.add_format({
        'bg_color': '#b2d8b2',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    worksheet.merge_range('A'+str(1)+':G'+str(1), 'REPORTE', format_title)
    worksheet.merge_range('A'+str(2)+':G'+str(2), 'USUARIOS', format_title)

    for i, subtitle in enumerate(subtitles):
        worksheet.write(2, i, subtitle, format_subtitle)

    for i, usuario in enumerate(usuarios):
        worksheet.write(3+i, 0, str(usuario['_id']), format_id)
        worksheet.write(3+i, 1, str(usuario['nombre']), format_nombre)
        worksheet.write(3+i, 2, str(usuario['correo']), format_correo)
        worksheet.write(3+i, 3, str(usuario['cedula']), format_cedula)
        worksheet.write(3+i, 4, str(usuario['telefono']), format_telefono)
        worksheet.write(3+i, 5, str(usuario['clave']), format_clave)
        worksheet.write(3+i, 6, str(usuario['huella_id']), format_huella)

    workbook.close()


def marcaciones_excel(filename, marcaciones):

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook('/root/api/reportes/'+str(filename)+'.xlsx')
    worksheet = workbook.add_worksheet('marcaciones')

    # Some data we want to write to the worksheet.
    subtitles = [
        'Nombre',
        'Huella ID',
        'Tipo',
        'Fecha',
        'Usuario ID'
    ]

    worksheet.set_column('A:A', 25)
    worksheet.set_column('B:B', 20)
    worksheet.set_column('C:C', 25)
    worksheet.set_column('D:D', 30)
    worksheet.set_column('E:E', 30)

    format_title = workbook.add_format({
        'bold': True,
        'bg_color': '#ffd700',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 30
    })

    format_subtitle = workbook.add_format({
        'bold': True,
        'bg_color': '#00cece',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 20
    })

    format_id = workbook.add_format({
        'bg_color': '#d1af78',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_nombre = workbook.add_format({
        'bg_color': '#42ba93',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_tipo = workbook.add_format({
        'bg_color': '#ffa500',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_fecha = workbook.add_format({
        'bg_color': '#ffedcc',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_huella = workbook.add_format({
        'bg_color': '#b2d8b2',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    worksheet.merge_range('A'+str(1)+':E'+str(1), 'REPORTE', format_title)
    worksheet.merge_range('A'+str(2)+':E'+str(2), 'MARCACIONES', format_title)

    for i, subtitle in enumerate(subtitles):
        worksheet.write(2, i, subtitle, format_subtitle)

    for i, marcacion in enumerate(marcaciones):
        worksheet.write(
            3+i, 0, str(marcacion['usuario']['nombre']), format_nombre)
        worksheet.write(3+i, 1, str(marcacion['huella_id']), format_huella)
        worksheet.write(3+i, 2, str(marcacion['tipo']), format_tipo)
        worksheet.write(3+i, 3, str(marcacion['creado_en']), format_fecha)
        worksheet.write(3+i, 4, str(marcacion['usuario']['_id']), format_id)

    workbook.close()


def entrada_excel(filename, marcaciones):

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook('/root/api/reportes/'+str(filename)+'.xlsx')
    worksheet = workbook.add_worksheet('entrada')

    # Some data we want to write to the worksheet.
    subtitles = [
        'Nombre',
        'Huella ID',
        'Fecha',
        'Usuario ID'
    ]

    worksheet.set_column('A:A', 25)
    worksheet.set_column('B:B', 20)
    worksheet.set_column('C:C', 30)
    worksheet.set_column('D:D', 30)

    format_title = workbook.add_format({
        'bold': True,
        'bg_color': '#ffd700',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 30
    })

    format_subtitle = workbook.add_format({
        'bold': True,
        'bg_color': '#00cece',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 20
    })

    format_id = workbook.add_format({
        'bg_color': '#d1af78',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_nombre = workbook.add_format({
        'bg_color': '#42ba93',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_fecha = workbook.add_format({
        'bg_color': '#ffedcc',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_huella = workbook.add_format({
        'bg_color': '#b2d8b2',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    worksheet.merge_range('A'+str(1)+':E'+str(1), 'REPORTE', format_title)
    worksheet.merge_range('A'+str(2)+':E'+str(2), 'ENTRADA', format_title)

    for i, subtitle in enumerate(subtitles):
        worksheet.write(2, i, subtitle, format_subtitle)

    for i, marcacion in enumerate(marcaciones):
        worksheet.write(
            3+i, 0, str(marcacion['usuario']['nombre']), format_nombre)
        worksheet.write(3+i, 1, str(marcacion['huella_id']), format_huella)
        worksheet.write(3+i, 2, str(marcacion['creado_en']), format_fecha)
        worksheet.write(3+i, 3, str(marcacion['usuario']['_id']), format_id)

    workbook.close()


def salida_almuerzo_excel(filename, marcaciones):

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook('/root/api/reportes/'+str(filename)+'.xlsx')
    worksheet = workbook.add_worksheet('salida_almuerzo')

    # Some data we want to write to the worksheet.
    subtitles = [
        'Nombre',
        'Huella ID',
        'Fecha',
        'Usuario ID'
    ]

    worksheet.set_column('A:A', 25)
    worksheet.set_column('B:B', 20)
    worksheet.set_column('C:C', 30)
    worksheet.set_column('D:D', 30)

    format_title = workbook.add_format({
        'bold': True,
        'bg_color': '#ffd700',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 30
    })

    format_subtitle = workbook.add_format({
        'bold': True,
        'bg_color': '#00cece',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 20
    })

    format_id = workbook.add_format({
        'bg_color': '#d1af78',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_nombre = workbook.add_format({
        'bg_color': '#42ba93',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_fecha = workbook.add_format({
        'bg_color': '#ffedcc',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_huella = workbook.add_format({
        'bg_color': '#b2d8b2',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    worksheet.merge_range('A'+str(1)+':E'+str(1), 'REPORTE', format_title)
    worksheet.merge_range('A'+str(2)+':E'+str(2),
                          'SALIDA ALMUERZO', format_title)

    for i, subtitle in enumerate(subtitles):
        worksheet.write(2, i, subtitle, format_subtitle)

    for i, marcacion in enumerate(marcaciones):
        worksheet.write(
            3+i, 0, str(marcacion['usuario']['nombre']), format_nombre)
        worksheet.write(3+i, 1, str(marcacion['huella_id']), format_huella)
        worksheet.write(3+i, 2, str(marcacion['creado_en']), format_fecha)
        worksheet.write(3+i, 3, str(marcacion['usuario']['_id']), format_id)

    workbook.close()


def regreso_almuerzo_excel(filename, marcaciones):

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook('/root/api/reportes/'+str(filename)+'.xlsx')
    worksheet = workbook.add_worksheet('regreso_almuerzo')

    # Some data we want to write to the worksheet.
    subtitles = [
        'Nombre',
        'Huella ID',
        'Fecha',
        'Usuario ID'
    ]

    worksheet.set_column('A:A', 25)
    worksheet.set_column('B:B', 20)
    worksheet.set_column('C:C', 30)
    worksheet.set_column('D:D', 30)

    format_title = workbook.add_format({
        'bold': True,
        'bg_color': '#ffd700',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 30
    })

    format_subtitle = workbook.add_format({
        'bold': True,
        'bg_color': '#00cece',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 20
    })

    format_id = workbook.add_format({
        'bg_color': '#d1af78',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_nombre = workbook.add_format({
        'bg_color': '#42ba93',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_fecha = workbook.add_format({
        'bg_color': '#ffedcc',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_huella = workbook.add_format({
        'bg_color': '#b2d8b2',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    worksheet.merge_range('A'+str(1)+':E'+str(1), 'REPORTE', format_title)
    worksheet.merge_range('A'+str(2)+':E'+str(2),
                          'REGRESO ALMUERZO', format_title)

    for i, subtitle in enumerate(subtitles):
        worksheet.write(2, i, subtitle, format_subtitle)

    for i, marcacion in enumerate(marcaciones):
        worksheet.write(
            3+i, 0, str(marcacion['usuario']['nombre']), format_nombre)
        worksheet.write(3+i, 1, str(marcacion['huella_id']), format_huella)
        worksheet.write(3+i, 2, str(marcacion['creado_en']), format_fecha)
        worksheet.write(3+i, 3, str(marcacion['usuario']['_id']), format_id)

    workbook.close()


def salida_excel(filename, marcaciones):

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook('/root/api/reportes/'+str(filename)+'.xlsx')
    worksheet = workbook.add_worksheet('salida')

    # Some data we want to write to the worksheet.
    subtitles = [
        'Nombre',
        'Huella ID',
        'Fecha',
        'Usuario ID'
    ]

    worksheet.set_column('A:A', 25)
    worksheet.set_column('B:B', 20)
    worksheet.set_column('C:C', 30)
    worksheet.set_column('D:D', 30)

    format_title = workbook.add_format({
        'bold': True,
        'bg_color': '#ffd700',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 30
    })

    format_subtitle = workbook.add_format({
        'bold': True,
        'bg_color': '#00cece',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 20
    })

    format_id = workbook.add_format({
        'bg_color': '#d1af78',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_nombre = workbook.add_format({
        'bg_color': '#42ba93',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_fecha = workbook.add_format({
        'bg_color': '#ffedcc',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    format_huella = workbook.add_format({
        'bg_color': '#b2d8b2',
        'align': 'center',
        'valign': 'vcenter',
        'border': True,
        'font_size': 12
    })

    worksheet.merge_range('A'+str(1)+':E'+str(1), 'REPORTE', format_title)
    worksheet.merge_range('A'+str(2)+':E'+str(2), 'SALIDA', format_title)

    for i, subtitle in enumerate(subtitles):
        worksheet.write(2, i, subtitle, format_subtitle)

    for i, marcacion in enumerate(marcaciones):
        worksheet.write(
            3+i, 0, str(marcacion['usuario']['nombre']), format_nombre)
        worksheet.write(3+i, 1, str(marcacion['huella_id']), format_huella)
        worksheet.write(3+i, 2, str(marcacion['creado_en']), format_fecha)
        worksheet.write(3+i, 3, str(marcacion['usuario']['_id']), format_id)

    workbook.close()
