
from datetime import date, datetime, timedelta
import time



class Convert():
    def str2timestamp(value):
        try:
            day = int(value[0:2])
            month = int(value[3:5])
            year = int(value[6:10])
            date_value = time.mktime(datetime.strptime(value, "%d/%m/%Y").timetuple())
        except:
            return None
        return date_value

    def timestamp2str(value):
        ts = int(value)
        return datetime.utcfromtimestamp(ts).strftime('%d/%m/%Y')

class TimeLocation():
    def get():
        now = datetime.utcnow() - timedelta(hours=5)
        return now

class RangeTime():
    def getTimestampPerDay():
        timestamp = TimeLocation.get()
        date_object = datetime.utcfromtimestamp(timestamp)

        day1 = str(date_object.day)+"/"+str(date_object.month)+"/"+str(date_object.year)
        day2 = str(date_object.day+1)+"/"+str(date_object.month)+"/"+str(date_object.year)

        start = time.mktime(datetime.strptime(day1, "%d/%m/%Y").timetuple())
        end = time.mktime(datetime.strptime(day2, "%d/%m/%Y").timetuple())

        return {
            'start': int(start),
            'end': int(end)
        }

    def getTimestampPerMonth():
        timestamp = TimeLocation.get()
        date_object = datetime.utcfromtimestamp(timestamp)

        day1 = "1/"+str(date_object.month)+"/"+str(date_object.year)
        day2 = "1/"+str(date_object.month+1)+"/"+str(date_object.year)

        start = time.mktime(datetime.strptime(day1, "%d/%m/%Y").timetuple())
        end = time.mktime(datetime.strptime(day2, "%d/%m/%Y").timetuple())

        return {
            'start': int(start),
            'end': int(end)
        }
    
    def getMonthYear():
        timestamp = TimeLocation.get()
        date_object = datetime.utcfromtimestamp(timestamp)
        return str(date_object.month)+"/"+str(date_object.year)

    def verify_date(start, end):
        timestamp = TimeLocation.get()
        if  timestamp >= start or timestamp <= end:
            return True
        else:
            return False

